# Patchnotes

## v1.1.0
- New theme for dice: DnD5e Red Box

## v1.0.1
- Folder colors work now.

## v1.0.0
- Initial release 
